# Base Image from Docker Hub
FROM maven

# Maintainer
LABEL maintainer="Saveyra <sodomgula.muni@saveyra.com>"

# Environment variable path for artifacts
ENV ARTIFACT_PATH="/opt/saveyra/artifacts"

# Create a directory for Java App
RUN mkdir -p $ARTIFACT_PATH

# Copy the current directory source code to /usr/src/myapp directoryy
COPY . $ARTIFACT_PATH

RUN ls -la

# Make the default work directory as /usr/src/myapp
WORKDIR $ARTIFACT_PATH

# Expose the 8080 port number to container
EXPOSE 8080

# Run container with generated build jar file ade
#CMD [ "sh", "-c", "java -jar $ARTIFACT_PATH/AvatarFeeder-0.0.1-SNAPSHOT.jar"]